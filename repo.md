# Git repo as package repo

You can use a git repository as a private (or public) PHP package repository.

Let's assume that `https://gitlab.com/daverona/templates/php.git` is the git repository of interest
and `daverona/aeon` is the vendor/package name pair of the package.

## Quick Start

### Listing

To list all tags, branches, or both:

```bash
# using SSH
git ls-remote --tags --sort="-version:refname" ssh://git@gitlab.com/daverona/templates/php.git   # tags only
git ls-remote --heads --sort="-version:refname" ssh://git@gitlab.com/daverona/templates/php.git  # branches only
git ls-remote --refs --sort="-version:refname" ssh://git@gitlab.com/daverona/templates/php.git   # both
# using HTTPS
git ls-remote --tags --sort="-version:refname" https://gitlab.com/daverona/templates/php.git   # tags only
git ls-remote --heads --sort="-version:refname" https://gitlab.com/daverona/templates/php.git  # branches only
git ls-remote --refs --sort="-version:refname" https://gitlab.com/daverona/templates/php.git   # both
```

### Installation

Before install, make `composer.json` file or add to it with the following:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "ssh://git@gitlab.com/daverona/templates/php.git"
    }
  ]
}
```

You may use HTTPS protocol for `url` value, i.e. `https://gitlab.com/daverona/templates/php.git`.

To install a tag, say `tag`:

```bash
composer require daverona/aeon:^tag
```

> Note that the highest tag will be installed if `:^tag` is omitted.

To install a branch, say `branch`:

```bash
composer require daverona/aeon:dev-branch
```

> Note that the highest *tag* not branch will be installed if `:dev-branch` is omitted.

To install a specific commit hash on a branch, say `hash` on `branch`:

```bash
composer require daverona/aeon:dev-branch#hash
```

> Note that a short hash (the first 7 characters of a commit hash) may be used instead of a commit hash.

Wondering why a branch is specified for a commit hash being unique? Here is an official answer:
[https://getcomposer.org/doc/04-schema.md#package-links](https://getcomposer.org/doc/04-schema.md#package-links)

## References

* git ls-remote: [https://git-scm.com/docs/git-ls-remote.html](https://git-scm.com/docs/git-ls-remote.html)
