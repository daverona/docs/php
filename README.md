# PHP

## Tutorials

* PHP Tutorial for Beginners: [https://youtu.be/pWG7ajC\_OVo](https://youtu.be/pWG7ajC_OVo)
* Object Oriented PHP Tutorial: [https://youtu.be/LuWxwLk8StM](https://youtu.be/LuWxwLk8StM)
* PHP Tutorial for Beginners (for Absolute Beginners): [https://youtu.be/yMclPkD4sQg](https://youtu.be/yMclPkD4sQg)
* PHP REST API Tutorial (Step by Step): [https://youtu.be/2EJ03wM0erI](https://youtu.be/2EJ03wM0erI)
* PHP CRUD (Create, Read, Update, Delete) Tutorial with MySQL + Bootstrap 4: [https://youtu.be/yifY51wjnSg](https://youtu.be/yifY51wjnSg)

## Style Guides 

* ~~PSR-0: Audoloading Standard: [https://www.php-fig.org/psr/psr-0/](https://www.php-fig.org/psr/psr-0/)~~
* PSR-1: Basic Coding Standard: [https://www.php-fig.org/psr/psr-1/](https://www.php-fig.org/psr/psr-1/)
* ~~PSR-2: Coding Style Guide: [https://www.php-fig.org/psr/psr-2/](https://www.php-fig.org/psr/psr-2/)~~
* PSR-4: Autoloader: [https://www.php-fig.org/psr/psr-4/](https://www.php-fig.org/psr/psr-4/)
* PSR-5: PHPDoc Standard (draft): [https://github.com/php-fig/fig-standards/blob/master/proposed/phpdoc.md](https://github.com/php-fig/fig-standards/blob/master/proposed/phpdoc.md)
* PSR-12: Extended Coding Style: [https://www.php-fig.org/psr/psr-12/](https://www.php-fig.org/psr/psr-12/)
* PSR-19: PHPDoc tags (draft): [https://github.com/php-fig/fig-standards/blob/master/proposed/phpdoc-tags.md](https://github.com/php-fig/fig-standards/blob/master/proposed/phpdoc-tags.md)

## Documentation

* PHP Manual: [https://www.php.net/manual/en/](https://www.php.net/manual/en/)
* Creating your first Composer/Packagist package: [https://blog.jgrossi.com/2013/creating-your-first-composer-packagist-package/](https://blog.jgrossi.com/2013/creating-your-first-composer-packagist-package/)
* Composer Documentation: [https://getcomposer.org/doc/](https://getcomposer.org/doc/)

## Linters

* pre-commit: [https://pre-commit.com/](https://pre-commit.com/)
* pre-commit Supported Hooks: [https://pre-commit.com/hooks.html](https://pre-commit.com/hooks.html)

## PhpStorm

* Remote debugging via SSH tunnel: [https://www.jetbrains.com/help/phpstorm/remote-debugging-via-ssh-tunnel.html](https://www.jetbrains.com/help/phpstorm/remote-debugging-via-ssh-tunnel.html)
* Debug your PHP in Docker with PhpStorm and Xdebug: [https://gist.github.com/chadrien/c90927ec2d160ffea9c4](https://gist.github.com/chadrien/c90927ec2d160ffea9c4)

## Packages

* asm89/stack-cors: [https://packagist.org/packages/asm89/stack-cors](https://packagist.org/packages/asm89/stack-cors)
* giggsey/libphonenumber-for-php: [https://packagist.org/packages/giggsey/libphonenumber-for-php](https://packagist.org/packages/giggsey/libphonenumber-for-php)
* intervention/image: [https://packagist.org/packages/intervention/image](https://packagist.org/packages/intervention/image)
* intervention/imagecache: [https://packagist.org/packages/intervention/imagecache](https://packagist.org/packages/intervention/imagecache)
* paragonie/csp-builder: [https://packagist.org/packages/paragonie/csp-builder](https://packagist.org/packages/paragonie/csp-builder)
* tecnickcom/tcpdf: [https://packagist.org/packages/tecnickcom/tcpdf](https://packagist.org/packages/tecnickcom/tcpdf)

### Development Only

* friendsofphp/php-cs-fixer: [https://packagist.org/packages/friendsofphp/php-cs-fixer](https://packagist.org/packages/friendsofphp/php-cs-fixer)
* phpmd/phpmd: [https://packagist.org/packages/phpmd/phpmd](https://packagist.org/packages/phpmd/phpmd)
* phpstan/phpstan: [https://packagist.org/packages/phpstan/phpstan](https://packagist.org/packages/phpstan/phpstan)
* squizlabs/php\_codesniffer: [https://packagist.org/packages/squizlabs/php\_codesniffer](https://packagist.org/packages/squizlabs/php_codesniffer)

## Troubleshooting

## References
