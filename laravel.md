# Laravel

## Tutorials

* Laravel 6 Tutorial for Beginners: [https://youtu.be/zckH4xalOns](https://youtu.be/zckH4xalOns)
* PHP Laravel Tutorial for Beginners: [https://youtu.be/OB8jBvu8N34](https://youtu.be/OB8jBvu8N34)

## Documentation

* Laravel Documentation: [https://laravel.com/docs/6.x](https://laravel.com/docs/6.x)
* Laravel API Reference: [https://laravel.com/api/6.x/](https://laravel.com/api/6.x/)

## Packages

* astrotomic/laravel-translatable: [https://packagist.org/packages/astrotomic/laravel-translatable](https://packagist.org/packages/astrotomic/laravel-translatable)
* davejamesmiller/laravel-breadcrumbs: [https://packagist.org/packages/davejamesmiller/laravel-breadcrumbs](https://packagist.org/packages/davejamesmiller/laravel-breadcrumbs)
* lab404/laravel-impersonate: [https://packagist.org/packages/lab404/laravel-impersonate](https://packagist.org/packages/lab404/laravel-impersonate)
* spatie/laravel-permission: [https://packagist.org/packages/spatie/laravel-permission](https://packagist.org/packages/spatie/laravel-permission)
* spinen/laravel-browser-filter: [https://packagist.org/packages/spinen/laravel-browser-filter](https://packagist.org/packages/spinen/laravel-browser-filter)
* venturecraft/revisionable: [https://packagist.org/packages/venturecraft/revisionable](https://packagist.org/packages/venturecraft/revisionable)
* wildside/userstamps: [https://packagist.org/packages/wildside/userstamps](https://packagist.org/packages/wildside/userstamps)

## Troubleshooting

## References
