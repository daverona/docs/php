# Lumen

## Documentation

* Lumen Documentation: [https://lumen.laravel.com/docs/6.x](https://lumen.laravel.com/docs/6.x)


## Packages

* dusterio/lumen-passport [https://packagist.org/packages/dusterio/lumen-passport](https://packagist.org/packages/dusterio/lumen-passport)

## Troubleshooting

* [https://medium.com/the-andela-way/setting-up-oauth-in-lumen-using-laravel-passport-2de9d007e0b0](https://medium.com/the-andela-way/setting-up-oauth-in-lumen-using-laravel-passport-2de9d007e0b0)

## References
